package biz.netcentric.badges.jordimorera.java8.greenbadge;

// Java program to demonstrate lamda expressions to implement
// a user defined functional interface.

@FunctionalInterface
interface Java8GreenBadgeFunctionalInterfaces
{
    int calculate(int x);
}

class FunctionalInterfaceTest
{
    public static void main(String args[])
    {
        int a = 5;

        // lambda expression to define the calculate method
        Java8GreenBadgeFunctionalInterfaces s = (int x)->x*x;

        // parameter passed and return type must be
        // same as defined in the prototype
        int ans = s.calculate(a);
        System.out.println(ans);
    }
}